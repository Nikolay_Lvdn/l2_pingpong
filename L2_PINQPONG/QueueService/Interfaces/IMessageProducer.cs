﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.Interfaces
{
    public interface IMessageProducer
    {
        void Send(string message, string type = null);
        void SendType(Type type, string message);
    }
}
