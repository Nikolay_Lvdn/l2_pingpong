﻿using QueueService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);
    }
}
