﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        IModel Channel { get; }
        void DeclareExchange(string exchangeName, string exchangeType);
        void BindQueue(string exhangeName, string routingKey, string queueName);
    }
}
