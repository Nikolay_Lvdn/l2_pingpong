﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.Interfaces
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
