﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public static class Settings
    {
        /*
 * (string exchangeNameForProducer, string queueNameForProducer, 
        string routingKeyForProducer, string exchangeNameForConsumer, 
        string queueNameForConsumer, string routingKeyForConsumer)
 */
        public static string HostName { get; }
        public static string ExchangeName { get; }
        public static string PingQueueName { get; }
        public static string PongQueueName { get; }
        public static string PingKey { get; }
        public static string PongKey { get; }
        public static string DefaultPingMessage { get; }
        public static string DefaultPongMessage { get; }
        public static int DefaultSleepTimeInMilliseconds { get; }
        static Settings()
        {
            HostName = "localhost";
            ExchangeName = "Exchange";
            PingQueueName = "Ping";
            PongQueueName = "Pong";
            PingKey = "ping";
            PongKey = "pong";
            DefaultPingMessage = "Ping!";
            DefaultPongMessage = "Pong!";
            DefaultSleepTimeInMilliseconds = 2500;
        }
    }
}
