﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.Models
{
    public class MessageConsumerSettings
    {
        public IModel Channel { get; internal set; }

        public bool SequentialFetch { get; set; } = true;

        public bool AutoAcknowledge { get; set; } = false;

        public string QueueName { get; set; }
    }
}
