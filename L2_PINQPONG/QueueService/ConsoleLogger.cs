﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public static class ConsoleLogger
    {
        public static void Print(Models.Action action, string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.Write($"{action}\t ");

            Console.ForegroundColor = ConsoleColor.Blue;

            Console.Write($"{message}\t");

            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.Write($"at {DateTime.Now:yyyy.MM.dd HH:mm:ss:ff}\n");

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
