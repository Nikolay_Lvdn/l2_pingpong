﻿using QueueService.Interfaces;
using QueueService.Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.QueueServices
{
    public class MessageQueue : IMessageQueue
    {
        private readonly IConnection _connection;
        public IModel Channel { get; protected set; }
        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings) : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);
            if (messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey, messageScopeSettings.QueueName);
            }
        }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void BindQueue(string exhangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exhangeName, routingKey);
        }

        public void Dispose()
        {
            Channel.Close();
            _connection.Close();
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
