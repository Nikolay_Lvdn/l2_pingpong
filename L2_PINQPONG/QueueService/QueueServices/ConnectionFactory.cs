﻿using BusinessLayer.Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueService.QueueServices
{
    public class ConnectionFactory
    {
        public static RabbitMQ.Client.ConnectionFactory MakeConection()
        {
            return new RabbitMQ.Client.ConnectionFactory() { HostName = Settings.HostName };
        }
    }
}
