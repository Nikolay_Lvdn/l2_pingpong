﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    interface IWrapper
    {
        void ListenQueue(Action sendMessage);
        void SendMessageToQueue(string message);
    }
}
