﻿using QueueService.Interfaces;
using QueueService.Models;
using QueueService.QueueServices;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using BusinessLayer;
using BusinessLayer.Models;

namespace RabbitMQ.Wrapper.Wrapper
{
    public class Wrapper : IWrapper
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;

        public Wrapper(string exchangeNameForProducer, string queueNameForProducer, 
            string routingKeyForProducer, string exchangeNameForConsumer, 
            string queueNameForConsumer, string routingKeyForConsumer)
        {
            IMessageProducerScopeFactory messageProducerScopeFactory = 
                new MessageProducerScopeFactory(QueueService.QueueServices.ConnectionFactory.MakeConection());

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = exchangeNameForProducer,
                QueueName = queueNameForProducer,
                RoutingKey = routingKeyForProducer,
                ExchangeType = ExchangeType.Direct,
            });

            IMessageConsumerScopeFactory messageConsumerScopeFactory = new MessageConsumerScopeFactory(QueueService.QueueServices.ConnectionFactory.MakeConection());

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = exchangeNameForConsumer,
                QueueName = queueNameForConsumer,
                RoutingKey = routingKeyForConsumer,
                ExchangeType = ExchangeType.Direct
            });
        }
        public void ListenQueue(System.Action sendMessage)
        {
            _messageConsumerScope.MessageConsumer.Received += (model, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body.ToArray());
                var routingKey = ea.RoutingKey;

                ConsoleLogger.Print(BusinessLayer.Models.Action.Receive, message);

                _messageConsumerScope.MessageConsumer.SetAcknowledge(ea.DeliveryTag, true);

                Thread.Sleep(Settings.DefaultSleepTimeInMilliseconds);
                sendMessage.Invoke();
            };
        }
        public void SendMessageToQueue(string message)
        {
            _messageProducerScope.MessageProducer.Send(message);

            ConsoleLogger.Print(BusinessLayer.Models.Action.Send, message);

        }
    }
}

