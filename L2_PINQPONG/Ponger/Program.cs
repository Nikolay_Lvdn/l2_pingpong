﻿using System;
using BusinessLayer.Models;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Wrapper;

namespace Ponger
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ponger is online! (P.S. Press any key to exit the program)\n");

            Wrapper wrapper = new Wrapper(Settings.ExchangeName, Settings.PingQueueName, Settings.PingKey, 
                Settings.ExchangeName, Settings.PongQueueName, Settings.PongKey);

            wrapper.ListenQueue(() =>
            {
                wrapper.SendMessageToQueue(Settings.DefaultPongMessage);
            });

            Console.ReadKey();
        }
    }
}
