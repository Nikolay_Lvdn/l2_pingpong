﻿using System;
using BusinessLayer.Models;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pinger is online! (P.S. Press any key to exit the program)\n");

            Wrapper wrapper = new Wrapper(Settings.ExchangeName, Settings.PongQueueName, Settings.PongKey,
                Settings.ExchangeName, Settings.PingQueueName, Settings.PingKey);

            wrapper.SendMessageToQueue(Settings.DefaultPingMessage);

            wrapper.ListenQueue(() =>
            {
                wrapper.SendMessageToQueue(Settings.DefaultPingMessage);
            });

            Console.ReadKey();
        }
    }
}
